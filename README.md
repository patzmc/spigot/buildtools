# BuildTools
This repository has a makefile to download and build the `Bukkit` and `Spigot` repositories  
First clone this repository to a directory
```bash
git clone https://gitlab.com/patzmc/spigot/buildtools <folder-name>
```

To Download and build use command:
```bash
make
```
