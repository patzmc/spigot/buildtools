FROM openjdk:8
RUN apt update
RUN apt install make
EXPOSE 25565
COPY Makefile .
RUN make
WORKDIR /server
RUN cp /spigot-1.8.8.jar .
ENTRYPOINT ["java", "-jar", "spigot-1.8.8.jar"]
