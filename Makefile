.PHONY: all
all: download build
download: clone-repositories
	if [ ! -f "BuildTools.jar" ] ; then wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar -O BuildTools.jar; fi
clone-repositories:
	if [ ! -d "BuildData" ] ; then git clone https://gitlab.com/patzmc/spigot/BuildData; fi
	if [ ! -d "Spigot" ] ; then git clone https://gitlab.com/patzmc/spigot/Spigot; fi
	if [ ! -d "Bukkit" ] ; then git clone https://gitlab.com/patzmc/spigot/Bukkit; fi
build:
	java -jar BuildTools.jar --rev 1.8.8 
